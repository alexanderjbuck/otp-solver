% otp puzzle solver
% Alex Buck

% Each suit is assigned a number
% A positive edge is a positive number, a negative edge is a negative number
% Club: 1/-1
% Diamond: 2/-2
% Heart: 3/-3
% Spade: 4/-4

% These are the puzzle pieces
piece([-4, 3, 1, -3]).
piece([-3, 4, 4, -1]).
piece([4, 3, -1, -4]).
piece([-3, 2, 4, -2]).
piece([-1, 2, 3, -1]).
piece([1, 2, -2, -1]).
piece([-2, 3, 1, -1]).
piece([-2, 2, 3, -3]).
piece([2, 4, -3, -4]).

% A puzzle piece can be rotated
rpiece(A) :- between(0, 3, N), piece(P), shift(N, P, A).

shift(N, Xs, Ys) :-
    length(Bs, N),
    append(As, Bs, Xs),
    append(Bs, As, Ys).

% A board consists of three rows and three columns, all pieces are unique
board([A,B,C,D,E,F,G,H,I]) :-
    row(A,B,C),
    col(A,D,G),
    row(D,E,F),
    col(B,E,H),
    row(G,H,I),
    col(C,F,I),
    piece_set([A,B,C,D,E,F,G,H,I]).

% A row has three pieces that fit horizontally without holes
row(A, B, C) :-
    rpiece(A), rpiece(B), rpiece(C),
    hfit(A, B), hfit(B, C).

% A column has three pieces that fit vertically without holes
col(A, B, C) :- 
    rpiece(A), rpiece(B), rpiece(C),
    vfit(A, B), vfit(B, C).

% Pieces fit if their facing edges fit
hfit([_, E, _, _], [_, _, _, W]) :- E =:= -W.
vfit([_, _, S, _], [N, _, _, _]) :- S =:= -N.

% A set of pieces contains unique pieces
piece_set([]).
piece_set([_]).
piece_set([H|T]) :- not(piece_in(H,T)), piece_set(T).

% Pieces must be unique when rotated in any direction
piece_in(P, [P2|_]) :- between(0, 3, N), shift(N, P, P2).
piece_in(P, [_|T]) :- piece_in(P, T).