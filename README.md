OTP Solver
=========
This is a prolog program that solves the One Tough Puzzle &#0153; puzzle.
Pieces are encoded by assigning each suit a number and giving a positive shape a
positive number and a negative shape a negative number.

To get the solution run this query:

    board(B).

Enjoy,

Alex
